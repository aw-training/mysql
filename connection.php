<?php

$servername = "localhost";
$username = "..";
$password = "..";

$conn = new mysqli($servername, $username, $password);

if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

echo "Connected successfully<br>";

$database = "CREATE DATABASE myDatabase";
if ($conn->query($database) === TRUE) {
  echo "Database created successfully";
} else {
  echo "Error creating database: " . $conn->error;
}

if($conn->close()) {
    echo "<br>Connection Closed";
}

?>
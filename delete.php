<?php
$servername = "localhost";
$username = "..";
$password = "..";
$database = "myDatabase";

$conn = new mysqli($servername, $username, $password, $database);

if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$delete = "DELETE FROM People WHERE id=3";

if ($conn->query($delete) === TRUE) {
  echo "Record deleted successfully";
} else {
  echo "Error deleting record: " . $conn->error;
}

$conn->close();
?>
<?php
$servername = "localhost";
$username = "..";
$password = "..";
$database = "myDatabase";

$conn = new mysqli($servername, $username, $password, $database);

if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$multi = "INSERT INTO People (firstname, lastname, email)
VALUES ('John', 'Doe', 'john@example.com');";
$multi .= "INSERT INTO People (firstname, lastname, email)
VALUES ('Mary', 'Moe', 'mary@example.com');";
$multi .= "INSERT INTO People (firstname, lastname, email)
VALUES ('Julie', 'Dooley', 'julie@example.com')";

if ($conn->multi_query($multi) === TRUE) {
  echo "New records created successfully";
} else {
  echo "Error: " . $multi . "<br>" . $conn->error;
}

$conn->close();
?> 